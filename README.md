# WARNING!!
This repo was forked from https://github.com/mtlynch/docker-sia and is in the process of still being updated.


## Supported Tags

* latest: The latest official binary release.

## Usage

```bash
mkdir sia-data
docker run \
  --detach \
  -e "PUID=4280" \
  -e "PGID=4280" \
  --volume $(pwd)/siaprime-data:/siaprime-data \
  --volume $(pwd)/.siaprime:/.siaprime \
  --publish 127.0.0.1:4280:4280 \
  --publish 4281:4281 \
  --publish 4282:4282 \
  --name siaprime-container \
  -e "SCPRIME_WALLET_PASSWORD=<Wallet_Password>" \
   piajesse/siaprime
```

**Important**: Never publish port 4280 to all interfaces. This is a security-sensitive API, so only expose it beyond 127.0.0.1 if you know what you're doing.

Once the container is running, you can execute spc from within the container:

```bash
$ docker exec -it siaprime-container ./spc consensus
Synced: No
Height: 3800
Progress (estimated): 2.4%
```

You can also call spd from outside the container:

```bash
$ curl -A "SiaPrime-Agent" "http://localhost:4280/consensus"
{"synced":false,"height":4690,"currentblock":"0000000000007d656e3bb0099737892b9073259cb05883b04c6f518fbf0faffb","target":[0,0,0,0,0,2,200,179,126,85,220,153,25,190,195,228,72,53,129,181,62,124,175,60,255,90,105,68,179,16,6,71],"difficulty":"101104922300609"}
```
