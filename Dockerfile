FROM debian:jessie-slim
LABEL maintainer="piajesse <me@piajesse.com>"

ARG SIA_VERSION="1.4.1"
ENV SIA_VERSION $SIA_VERSION
ENV SIA_PACKAGE="SiaPrime-v${SIA_VERSION}-linux-amd64"
ARG SIA_ZIP="${SIA_PACKAGE}.zip"
ENV SIA_ZIP $SIA_ZIP
ARG SIA_RELEASE="https://siaprime.net/releases/${SIA_ZIP}"
ARG SIA_DIR="/siaprime"
ENV SIA_DIR $SIA_DIR
ENV SIA_DATA_DIR="/siaprime-data"
ENV SIA_MODULES gctwhr
ENV PUID=4280
ENV PGID=4280

RUN apt update && \
        apt install -y \
        wget \
        unzip \
        sudo \
        socat

RUN groupadd -g $PGID siaprimegroup && \
        useradd -u $PUID -g siaprimegroup siaprime
  

RUN wget "$SIA_RELEASE" && \
        mkdir "$SIA_DIR" && \
        unzip -j "$SIA_ZIP" "${SIA_PACKAGE}/spc" -d "$SIA_DIR" && \
        unzip -j "$SIA_ZIP" "${SIA_PACKAGE}/spd" -d "$SIA_DIR"

# Clean up.
RUN apt-get remove -y wget unzip && \
        rm "$SIA_ZIP" && \
        rm -rf /var/lib/apt/lists/* && \
        rm -Rf /usr/share/doc && \
        rm -Rf /usr/share/man && \
        apt-get autoremove -y && \
        apt-get clean

EXPOSE 4281 4282

WORKDIR "$SIA_DIR"
ENTRYPOINT socat tcp-listen:4280,reuseaddr,fork tcp:localhost:8000 & \
        sudo -u \#$PUID -g \#$PGID \
        "SCPRIME_WALLET_PASSWORD=$SCPRIME_WALLET_PASSWORD" \
        ./spd \
        --modules "$SIA_MODULES" \
        --siaprime-directory "$SIA_DATA_DIR" \
        --api-addr "localhost:8000"
